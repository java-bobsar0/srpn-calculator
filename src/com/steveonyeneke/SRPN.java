package com.steveonyeneke;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;

public class SRPN {
    private Stack<Integer> operandStack;
    private Stack<ValidOperator> operatorStack;
    private final String validOperators;

    // Initialize comment tracker
    private boolean hasCommentStarted = false;

    // Used to denote the index of the operand in the operandStack where we can begin multiline(immediate) calculations based o operator precedence
    int globalStartOperandIndex = -1;

    private final RandomNumberGen randomNumberGen;

    public SRPN() {
        operandStack = new Stack<>();
        operatorStack = new Stack<>();
        // Initialize string containing valid operators according to operator precedence
        validOperators = ValidOperator.getValidOperators;
        // Instantiate RandomNumberGen with seed of 1 to match the sample code output.
        randomNumberGen = new RandomNumberGen(1000, 1);
    }

    /**
     * Process the single line of character(s) inputted by the user
     *
     * @param s - single line of user input as {@code String}
     */
    public void processCommand(String s) {
        if (s.isBlank()) {
            return;
        }
        // Convert the string to an array using a whitespace delimiter to detect for the presence of a comment
        // A comment starts with "# " and ends with " #" so a "#" in the array will indicate the start or end of a comment
        String[] words = s.trim().split(" ");

        for (String word : words) {
            if (word.equals("#")) {
                //Toggle comment tracker i.e if comment has already started, turn off comment tracker; else turn it on
                hasCommentStarted = !hasCommentStarted;
                continue;
            }
            if (!hasCommentStarted) {
                processWord(word);
            }
        }
    }

    // Process a single word - note that a word doesn't contain a whitespace character.
    private void processWord(String word) {
        try {
            //Use a string builder to concatenate single digits to form an integer
            StringBuilder intStrBuilder = new StringBuilder();
            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);

                if (Character.isDigit(c)) {
                    intStrBuilder.append(c);
                    continue;
                }
                //If char is not a digit, convert contents of intStrBuilder to integer and push to operandStack ready for calculation
                if (intStrBuilder.length() > 0) {
                    pushIntToOperandStack(convertToInt(intStrBuilder.toString()));
                    //Empty StringBuilder to prepare for next incoming digits
                    intStrBuilder.setLength(0);
                }
                if (validOperators.indexOf(c) != -1) {
                    ValidOperator validOperatorObj = new ValidOperator(c);

                    //If there's a digit following the operator, set operateImmediately to true. This doesnt include the minus sign as it is a special case
                    if (i != word.length() - 1 && Character.isDigit(word.charAt(i + 1)) && c != '-') {
                        validOperatorObj.setOperateImmediately(true);
                        //If there's something in stringBuilder get the index of the last added element as globalStartIndex in operandStack if globalIndex has not previously been assigned
                        if (globalStartOperandIndex == -1) {
                            globalStartOperandIndex = operandStack.size() - 1;
                        }
                    }
                    //Special case: If the minus operator is the first char and immediately followed by a digit, add minus to strBuilder so the number in strBuilder is negative
                    if (c == '-' && i == 0 && i != word.length() - 1 && Character.isDigit(word.charAt(i + 1))) {
                        intStrBuilder.append(c);
                    } else {
                        operatorStack.add(validOperatorObj);
                    }
                    continue;
                }

                handleSpecialCharacters(intStrBuilder, c);
            }

            //If there's any digits left in stringBuilder, push to operandStack
            if (intStrBuilder.length() > 0) {
                pushIntToOperandStack(convertToInt(intStrBuilder.toString()));
            }
            if (operatorStack.size() > 0) {
                processStackOperandsWithOperator();
            }
        } catch (StackOverflowError | ArithmeticException | NumberFormatException e) {
            System.out.println(e.getMessage());
        } catch (EmptyStackException e) {
            System.out.println(ErrorMessages.STACK_EMPTY);
        }
    }

    //Handles special characters as determined by the sample program.
    //Chars include: '=': returns last element in operandStack, 'd': prints entire operandStack and 'r': returns random numbers. Any other char is printed as invalid/unknown
    private void handleSpecialCharacters(StringBuilder intStrBuilder, char c) {
        switch (c) {
            case '=':
                if (intStrBuilder.length() > 0) {
                    System.out.println(intStrBuilder);
                } else { //Look at the element at the top of the stack without removing it.
                    System.out.println(operandStack.peek());
                }
                break;
            case 'd':
                if (operatorStack.size() > 0) {
                    processStackOperandsWithOperator();
                }
                printOperandStack();
                break;
            case 'r':
                pushIntToOperandStack(randomNumberGen.getRandomInteger());
                break;
            default:
                System.out.println(ErrorMessages.getInvalidCharErrorMsg(c));
        }
    }

    private void printOperandStack() {
        if (operandStack.isEmpty()) {
            System.out.println(Integer.MIN_VALUE);
        } else {
            for (Integer number : operandStack) {
                System.out.println(number);
            }
        }
    }

    private void pushIntToOperandStack(int integer) {
        //Max size reflects that in sample program
        int stackMaxSize = 23;
        if (operandStack.size() == stackMaxSize) {
            throw new StackOverflowError(ErrorMessages.STACK_OVERFLOW);
        }
        operandStack.push(integer);
    }

    // Replaces the elements at firstOperandIndex and firstOperandIndex+1 (2 operands) with the number
    private void replaceOperandsInStack(int number, int firstOperandIndex) {
        try {
            operandStack.set(firstOperandIndex, number);
            operandStack.remove(firstOperandIndex + 1);
        } catch (Exception ignored) {
        }
    }

    // Converts a String containing a number to an integer
    private int convertToInt(String numberStr) {
        if (numberStr.isBlank()) {
            throw new NumberFormatException();
        }
        // Convert the numberStr to a BigInteger to allow for comparison with max and min integer values
        BigInteger bigNumberStr = new BigInteger(numberStr);
        return getIntFromBigNumber(bigNumberStr);
    }

    private int getIntFromBigNumber(BigInteger bigInt) {
        BigInteger bigMaxIntValue = BigInteger.valueOf(Integer.MAX_VALUE);
        BigInteger bigMinIntValue = BigInteger.valueOf(Integer.MIN_VALUE);

        //Handle saturation - if bigInt is > max int value, return max int value; if < min int value, return min int value
        if (bigInt.compareTo(bigMaxIntValue) > 0) {
            return Integer.MAX_VALUE;
        } else if (bigInt.compareTo(bigMinIntValue) < 0) {
            return Integer.MIN_VALUE;
        } else {
            return bigInt.intValueExact();
        }
    }

    //Method overloading - same as above but takes in a bigDecimal
    private int getIntFromBigNumber(BigDecimal bigDecimal) {
        BigDecimal bigMaxIntValue = BigDecimal.valueOf(Integer.MAX_VALUE);
        BigDecimal bigMinIntValue = BigDecimal.valueOf(Integer.MIN_VALUE);

        if (bigDecimal.compareTo(bigMaxIntValue) > 0) {
            return Integer.MAX_VALUE;
        } else if (bigDecimal.compareTo(bigMinIntValue) < 0) {
            return Integer.MIN_VALUE;
        } else {
            return bigDecimal.intValueExact();
        }
    }

    // Obtains the 2 operands and operator needed for an operation, calculates and inserts result in stack
    private void processStackOperandsWithOperator() {

        for (int i = 0; i < validOperators.length(); i++) {
            //Check if any valid operators are present in the stack to be used for calculation.
            if (operatorStack.size() == 0) {
                break;
            }
            char operator = validOperators.charAt(i);

            getOperandsAndOperateOnStack(operator);
        }
        // clear any left over operators in stack to leave stack empty for next iteration
        operatorStack.clear();
        // reset globalStartOperandIndex
        globalStartOperandIndex = -1;
    }

    //Retrieves operands to be used for calculation and calculates based on operator precedence already defined by validOperators
    private void getOperandsAndOperateOnStack(char operator) {
        //Get count of times an operator character appears in operator stack
        ArrayList<ValidOperator> validOperatorList = getSimilarOperatorsInStack(operator);

        for (ValidOperator validOperator : validOperatorList) {
            //Print a stack underflow error when there's not enough operands (at least 2) in stack to perform an operation.
            if (operandStack.size() < 2) {
                System.out.println(ErrorMessages.STACK_UNDERFLOW);
                continue;
            }
            int operatorStackIndex = operatorStack.indexOf(validOperator);

            try {
                //The first element in the operatorStack that calculation will be done on. This must be at least +1 of size of operators
                int operandStartIndex = operandStack.size() - (operatorStack.size() + 1);
                if (validOperator.isOperateImmediately()) {
                    operandStartIndex = globalStartOperandIndex;
                }
                //Get the 2 operands needed for calculation based on an occurrence of the operator in operatorStackIndex
                int firstOperandIndex = operandStartIndex + operatorStackIndex;

                int firstOperand = operandStack.elementAt(firstOperandIndex);
                int secondOperand = operandStack.elementAt(firstOperandIndex + 1);

                if (operator == '/' && secondOperand == 0) {
                    System.out.println(ErrorMessages.DIVIDE_BY_ZERO);
                } else {
                    BigDecimal bigResult = calculate(firstOperand, secondOperand, operator);
                    replaceOperandsInStack(getIntFromBigNumber(bigResult), firstOperandIndex);
                    //Remove operator in index from operatorStack to prevent re-calculating with same operator
                    operatorStack.remove(operatorStackIndex);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                operatorStack.clear();
                System.out.println(ErrorMessages.STACK_UNDERFLOW);
            }
            //Reset operateImmediately value
            validOperator.setOperateImmediately(false);
        }
    }

    // Returns an ArrayList of validOperators in operatorStack that contain a certain operator
    private ArrayList<ValidOperator> getSimilarOperatorsInStack(char operator) {
        ArrayList<ValidOperator> validOperators = new ArrayList<>();

        for (ValidOperator validOperator : operatorStack) {
            if (validOperator.getOperator() == operator) {
                validOperators.add(validOperator);
            }
        }
        return validOperators;
    }

    /**
     * Performs calculation on two operands based on operator.
     *
     * @param x        first operand for calculation.
     * @param y        second operand for calculation.
     * @param operator operator to be used on the operands.
     * @return The result of the calculation in {@code BigDecimal}
     */
    public BigDecimal calculate(int x, int y, char operator) {
        BigDecimal bigX = BigDecimal.valueOf(x);
        BigDecimal bigY = BigDecimal.valueOf(y);
        switch (operator) {
            case '/':
                return BigDecimal.valueOf(x / y);
            case '*':
                return bigX.multiply(bigY);
            case '+':
                return bigX.add(bigY);
            case '-':
                return bigX.subtract(bigY);
            case '%':
                return bigX.remainder(bigY);
            case '^':
                return bigX.pow(y);
            default:
                throw new ArithmeticException(ErrorMessages.getInvalidCharErrorMsg(operator));
        }
    }
}
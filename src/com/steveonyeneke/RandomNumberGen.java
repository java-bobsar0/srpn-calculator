package com.steveonyeneke;

/**
 * Random number generator class.
 * Constructor takes in size of array, initial seed and initializes a tracker of the random number to be outputted
 */
public class RandomNumberGen {

    private final int[] randomNumberArr;
    private final int initialSeed;
    private int tracker;

    /**
     * Instantiates RandomNumberGen class and generates an array of random numbers based on desired count
     * @param size
     *          size of the random numbers to be generated
     * @param seed
     *          initial number to be seeded into the empty random number array. This affects how the random numbers are generated.
     */
    public RandomNumberGen(int size, int seed) {
        initialSeed = seed;
        tracker = 0;
        randomNumberArr = new int[size];

        // generate array that stores the random numbers
        generateRandomNumberArr();
    }

    // Generate random number array using the GLIBC random number generator
    // Implemented based on the mathematical description of algorithm by Peter Selinger at https://mathstat.dal.ca/~selinger/random (Jan 4, 2007)
    private void generateRandomNumberArr(){
        randomNumberArr[0] = initialSeed;
        final long long16807 = 16807;

        for(int i=1; i<=30; i++){
            int val = (int) ((long16807 * randomNumberArr[i-1]) % Integer.MAX_VALUE);
            randomNumberArr[i] = val;

            if(randomNumberArr[i] < 0) {
                randomNumberArr[i] += Integer.MAX_VALUE;
            }
        }
        // copy contents of arr in positions 0-2 to positions 31-33
        System.arraycopy(randomNumberArr, 0, randomNumberArr, 31, 3);

        for(int i=34; i<randomNumberArr.length; i++){
            randomNumberArr[i] = randomNumberArr[i-3] + randomNumberArr[i-31];
        }
    }

    /**
     * Retrieves a random integer in the randomNumberArr based on user requests.
     * A tracker is incremented whenever this function is called. This is typically when user requests for a random number in the console.
     *
     * @return The random {@code int} based on the number of times this function is called.
     */
    public int getRandomInteger(){
        int randomInt = randomNumberArr[tracker+344] >> 1;
        if(randomInt < 0){
            randomInt += Integer.MAX_VALUE + 1;
        }
        tracker++;
        return randomInt;
    }
}
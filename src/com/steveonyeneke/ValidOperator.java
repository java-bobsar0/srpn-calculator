package com.steveonyeneke;

/**
 * A wrapper class around an operator character that is considered valid.
 * Valid operators are used for calculations and includes an operateImmediately field which indicates if calculation should happen immediately in the case of string containing multiple operations
 */
public class ValidOperator {
    private final char operator;
    private boolean operateImmediately;

    // String containing valid operators according to operator precedence
    static final String getValidOperators = "^/*+-%";

    public ValidOperator(char operator) {
        this.operator = operator;
        operateImmediately = false;
    }

    /**
     * Gets the valid operator character that is contained within the object
     * @return the {@code char} valid operator
     */
    public char getOperator() {
        return operator;
    }

    public boolean isOperateImmediately() {
        return operateImmediately;
    }

    /**
     * Sets the value of operateImmediately to true or false
     * @param operateImmediately
     *                          the boolean value to set.
     */
    public void setOperateImmediately(boolean operateImmediately) {
        this.operateImmediately = operateImmediately;
    }
}

package com.steveonyeneke;

/**
 * A class that returns the different error messages that might be encountered in the program
 */
public class ErrorMessages {
    /**
     * Creates a custom [Unrecognised operator or operand] error message based on an invalid operator or operand
     * @param op
     *        the operator or operand to output with the error message
     * @return The resulting error message {@code String}
     */
    public static String getInvalidCharErrorMsg(char op) {
        return String.format("Unrecognised operator or operand \"%c\".", op);
    }

    public static final String STACK_UNDERFLOW = "Stack underflow.";

    public static final String STACK_OVERFLOW = "Stack overflow.";

    public static final String STACK_EMPTY = "Stack overflow.";

    public static final String DIVIDE_BY_ZERO = "Divide by 0.";
}
